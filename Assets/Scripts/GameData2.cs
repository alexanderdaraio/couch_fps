﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameData2 : MonoBehaviour {

	public static GameData2 Data;

	//set in editor
	public GameObject player;

	//terrain information
	float[,,] brickVals;
	bool[,,] bricks;
	GameObject[,,] brickObjects;
	// List of spawn points
	List<GameObject> brickSpawnObj;

	void Awake(){
		Data = this;
		brickSpawnObj = new List<GameObject> ();
		GetComponent<TerrainGen> ().Generate(out brickVals, out bricks);
	}

	void Start(){
		GetComponent<TerrainGen> ().Create(out brickObjects);
		int res = GetComponent<TerrainGen> ().resk;
		SetPlayer (res, res, res/2);
	}
		
	public bool GetBrick(int i, int j, int k){
		return bricks[i,j,k];
	}

	public float GetBrickValue(int i, int j, int k){
		return brickVals[i,j,k];
	}

	public void StoreSpawn(GameObject brick){
		brickSpawnObj.Add (brick);
	}

	//make this better
	public void SetPlayer(int i, int j, int k){
		Instantiate (player, new Vector3 (1.817f*(float)i, (float)j, 2*(float)k), Quaternion.identity);

	}

}

