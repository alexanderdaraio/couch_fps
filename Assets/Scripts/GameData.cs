﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameData : MonoBehaviour{
	public static GameData Data;
	public 	Dictionary<string, Material> materialType;
	public GameObject flag;
	public GameObject player;
	// defaults to false
	bool[,,] bricks;
	int res;
	//if you need to cycle through blocks for some reason (ie on a refresh, but just use raycast), use this
	// List of spawn points
	List<GameObject> brickSpawnObj;

	void Awake(){
		Data = this;
		materialType = new Dictionary<string, Material> ();
		Createtypes ();
		brickSpawnObj = new List<GameObject> ();
		SetPlayer (16, 32, 16);
	}

	public void Setup(int res){
		bricks = new bool[res,res,res];
		this.res = res;
	}
	public void SetBrick(int i, int j, int k, bool value){
		bricks[i,j,k]= value;
	}
	public bool GetBrick(int i, int j, int k){
		return bricks[i,j,k];
	}

	public void SetFlag(int i, int j, int k){
		GameObject flag1;
		if (i % 2 == 0) {
			flag1 = Instantiate(flag, new Vector3(1.817f*(float)i+1.225f,j+1f, 2*(float)k+1f+1f), transform.rotation) as GameObject;
		} else {
			flag1 = Instantiate(flag, new Vector3(1.817f*(float)i+1.225f,j+1f, 2*(float)k+1f), transform.rotation) as GameObject;
		}
	

	}

	//make this better
	public void SetPlayer(int i, int j, int k){
		Instantiate (player, new Vector3 (1.817f*(float)i, (float)j, 2*(float)k), Quaternion.identity);
	
	}

	// sets up type of material that can be used in game
	void Createtypes(){
		Material dirtMat = Resources.Load ("Materials/Dirt", typeof(Material)) as Material;
		Material grassMat = Resources.Load ("Materials/Grass", typeof(Material)) as Material;
		Material rockMat = Resources.Load ("Materials/Rock", typeof(Material)) as Material;
		Material sideGrassMat = Resources.Load ("Materials/SideGrass", typeof(Material)) as Material;
		materialType.Add ("dirt", dirtMat);
		materialType.Add ("grass", grassMat);
		materialType.Add ("sideGrass", sideGrassMat);
		materialType.Add ("rock", rockMat);
	}

	void SpawnPlayer(){
		//find the highest weight based on distance to flag, height, and distance to other players spawned from brickobj list. 

	}

	public void StoreSpawn(GameObject brick){
		brickSpawnObj.Add (brick);
	}

	public Material getMaterial(string name){
		return materialType [name];
	}



}
