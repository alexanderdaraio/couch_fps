﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


abstract public class Gun : MonoBehaviour {

	// shoots bullet
	public abstract void shoot ();

}
