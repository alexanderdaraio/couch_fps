﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunTypeShotgun : Gun {

	public Transform bullet;
	public int bulletNum;
	public int bulletSpreadAngle;
	public float gunLength;
	public float gunRadius;

	// shoots shotgun pellets
	public override void shoot () {

		Vector3 gunLengthOffset =  Vector3.forward * gunLength / 2 + Vector3.forward *.4f;


		//variable to make middle bullet and others always line up nicely
		int g = 0;
		if (bulletNum!=2) {
			g = 1;
			// first bullet in the centre
			Transform.Instantiate (bullet, transform.TransformPoint(gunLengthOffset), transform.rotation);
		}

		
		// the angle difference around the z axis between each of the bullets for position and direction calculation. 
		float angle = 2*Mathf.PI  / (bulletNum - (float)g);
		for (int i = 0; i < bulletNum - g; i++) {
			Vector3 posChange = new Vector3 (Mathf.Cos (angle*i), Mathf.Sin (angle*i), 0f);
			Vector3 spawnPointLocal =  posChange * gunRadius * 4f / 5f + gunLengthOffset;
			//not doing anything???
			Quaternion zRotation = Quaternion.Euler (new Vector3 (0,0, Mathf.Rad2Deg*(angle * i)));
			Quaternion yRotation = Quaternion.Euler (new Vector3 (0,bulletSpreadAngle,0));
			Transform.Instantiate (bullet, transform.TransformPoint(spawnPointLocal), transform.rotation *zRotation*yRotation);

		}
		
	}
}
