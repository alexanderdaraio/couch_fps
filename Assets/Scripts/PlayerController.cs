﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {
	public GameObject gun;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		// shoots gun irrespective, so long as class type Gun
		if(Input.GetMouseButtonUp(0)){
			gun.GetComponent<Gun>().shoot();
		}
	}
}
