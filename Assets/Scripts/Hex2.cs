﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hex2 : MonoBehaviour {
	string type;
	int posI,posJ,posK;
	int resi,resj,resk;
	float value;
	//maybe we should render on start and use a yield or something
	void Start(){
		CheckRender ();
	}
	//sets the position of each brick in space (basically acts as its unique id)
	public void SetPos(int i, int j, int k, int resi,int resj,int resk, float value){
		posI = i;
		posJ = j;
		posK = k;
		this.resi = resi;
		this.resj = resj;
		this.resk = resk;
		this.value = value;
	}

	// Checks if the cube needs to show its renderer. Default is off 
	void CheckRender () {

		//checks if it is on the edge or if squares around it are taken (for render on all sides), and grass and tress on top
		//above or the very top
		//each possible rendered edge is numbered so that their materials can be different
		if (posJ == resj-1 ||  !GameData2.Data.GetBrick (posI, posJ+1, posK)) {
			Render (0);
			//place tree code here
			//make sure the top and sides render the right texture
			if (posJ+1 >= resj-1 || !GameData2.Data.GetBrick (posI, posJ+2, posK)) {
				//store these locations as possible spawn locations
				GameData2.Data.StoreSpawn(this.gameObject);
			}
			//setMaterial(0);
		}
		// even and odd rendering differences
		if (posI % 2 != 0) {
			//forward right
			if ((posI== resi - 1) ||!GameData2.Data.GetBrick(posI + 1, posJ, posK)) {
				Render (5);
			}
			//back right
			if ((posI== resi - 1) || (posK== 0) || !GameData2.Data.GetBrick (posI + 1, posJ, posK-1)) {
				Render (4);
			}

			//forward left
			if ((posI == 0) ||!GameData2.Data.GetBrick (posI - 1, posJ, posK)) {
				Render (7);
			}

			//back left
			if ((posI == 0) || (posK== 0) ||!GameData2.Data.GetBrick (posI - 1, posJ, posK-1)) {
				Render (2);
			}

		} else {
			//forward right
			if ((posI== resi - 1) || (posK== resk - 1) || !GameData2.Data.GetBrick (posI + 1, posJ, posK+1)) {
				Render (5);
			}
			//back right
			if ((posI== resi - 1) ||!GameData2.Data.GetBrick (posI + 1, posJ, posK)) {
				Render (4);
			}

			//forward left
			if ((posI == 0) ||(posK== resk - 1) || !GameData2.Data.GetBrick (posI - 1, posJ, posK+1)) {
				Render (7);
			}

			//back left
			if ((posI == 0) ||!GameData2.Data.GetBrick (posI - 1, posJ, posK)) {
				Render (2);
			}

		}


		//forward
		if (posK == resk-1 || !GameData2.Data.GetBrick (posI, posJ, posK+1)) {
			Render (6);
		}

		//back
		if (posK == 0 || !GameData2.Data.GetBrick (posI, posJ, posK-1)) {
			Render (3);
		}
		//bottom
		if ((posJ == 0) || !GameData2.Data.GetBrick (posI, posJ - 1, posK)) {
			Render (1);
		}

	}



	void setMaterial(int i){



		//Dont set material if it has already been set
		/* if (string.Equals(type, "grass")) {
			if (i !=0 && i!=1) {
				transform.GetChild (i).GetComponent<Renderer> ().material = GameData.Data.getMaterial ("sideGrass");
			} else if (i == 1) {
				transform.GetChild (i).GetComponent<Renderer> ().material = GameData.Data.getMaterial ("dirt");
			} else {
				transform.GetChild (i).GetComponent<Renderer> ().material = GameData.Data.getMaterial ("grass");;
			}
		}
		else{
			transform.GetChild (i).GetComponent<Renderer> ().material = GameData.Data.getMaterial (type);
		}	*/
	}

	//enables/disables renderer & collider
	void Render (int x) {
		transform.GetChild (x).GetComponent<Renderer> ().enabled = true;
		transform.GetChild (x).GetComponent<MeshCollider> ().enabled = true;
		setMaterial(x);
	}
	void PlaceTree(int i, int j, int k){

	}
}
