﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Hex : MonoBehaviour {
	public float rockVal = 0;
	string type;
	int posI,posJ,posK;
	int res;
	float perlinVal;
	//maybe we should render on start and use a yield or something
	void Start(){
		Checktype ();
		CheckRender ();
	}
	//sets the position of each brick in space (basically acts as its unique id)
	public void SetPos(int i, int j, int k, int res, float perlinVal){
		posI = i;
		posJ = j;
		posK = k;
		this.res = res;
		this.perlinVal = perlinVal;
	}

	// Checks if the cube needs to show its renderer. Default is off 
	void CheckRender () {

		//checks if it is on the edge or if squares around it are taken (for render on all sides), and grass and tress on top
		//above or the very top
		if (posJ == res-1 || !GameData.Data.GetBrick (posI, posJ+1, posK)) {
			Render (0);
			//place tree code here
			//make sure the top and sides render the right texture
			if (posJ+1 >= res-1 || !GameData.Data.GetBrick (posI, posJ+2, posK)) {
				//store these locations as possible spawn locations
				GameData.Data.StoreSpawn(this.gameObject);
			}
			//setMaterial(0);
		}
		// even and odd rendering differences
		if (posI % 2 != 0) {
			//forward right
			if ((posI== res - 1) ||!GameData.Data.GetBrick (posI + 1, posJ, posK)) {
				Render (5);
				//setMaterial(5);
			}
			//back right
			if ((posI== res - 1) || (posK== 0) || !GameData.Data.GetBrick (posI + 1, posJ, posK-1)) {
				Render (4);
				//setMaterial(4);
			}

			//forward left
			if ((posI == 0) ||!GameData.Data.GetBrick (posI - 1, posJ, posK)) {
				Render (7);
				//setMaterial(7);
			}

			//back left
			if ((posI == 0) || (posK== 0) ||!GameData.Data.GetBrick (posI - 1, posJ, posK-1)) {
				Render (2);
				//setMaterial(2);
			}

		} else {
			//forward right
			if ((posI== res - 1) || (posK== res - 1) || !GameData.Data.GetBrick (posI + 1, posJ, posK+1)) {
				Render (5);
				//setMaterial(5);
			}
			//back right
			if ((posI== res - 1) ||!GameData.Data.GetBrick (posI + 1, posJ, posK)) {
				Render (4);
				//setMaterial(4);
			}

			//forward left
			if ((posI == 0) ||(posK== res - 1) || !GameData.Data.GetBrick (posI - 1, posJ, posK+1)) {
				Render (7);
				//setMaterial(7);
			}

			//back left
			if ((posI == 0) ||!GameData.Data.GetBrick (posI - 1, posJ, posK)) {
				Render (2);
				//setMaterial(2);
			}

		}


		//forward
		if (posK == res-1 || !GameData.Data.GetBrick (posI, posJ, posK+1)) {
			Render (6);
			//setMaterial(6);
		}

		//back
		if (posK == 0 || !GameData.Data.GetBrick (posI, posJ, posK-1)) {
			Render (3);
			//setMaterial(3);
		}
		//bottom
		if ((posJ == 0) || !GameData.Data.GetBrick (posI, posJ - 1, posK)) {
			Render (1);
			//setMaterial(1);
		}

	}


	void Checktype(){
		if (perlinVal < rockVal) {
			type = "rock";
		} else if (posJ == res-1 || !GameData.Data.GetBrick (posI, posJ+1, posK)) {
			type = "grass";
		} else {
			type = "dirt";
		}
	}

	void setMaterial(int i){
		//Dont set material if it has already been set
		if (string.Equals(type, "grass")) {
			if (i !=0 && i!=1) {
				transform.GetChild (i).GetComponent<Renderer> ().material = GameData.Data.getMaterial ("sideGrass");
			} else if (i == 1) {
				transform.GetChild (i).GetComponent<Renderer> ().material = GameData.Data.getMaterial ("dirt");
			} else {
				transform.GetChild (i).GetComponent<Renderer> ().material = GameData.Data.getMaterial ("grass");;
			}
		}
		else{
			transform.GetChild (i).GetComponent<Renderer> ().material = GameData.Data.getMaterial (type);
		}	
	}

	//enables/disables renderer & collider
	void Render (int x) {
		transform.GetChild (x).GetComponent<Renderer> ().enabled = true;
		transform.GetChild (x).GetComponent<MeshCollider> ().enabled = true;
		setMaterial(x);
	}
	void PlaceTree(int i, int j, int k){

	}
}
