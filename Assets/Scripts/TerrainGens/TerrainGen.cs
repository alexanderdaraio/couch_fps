﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

abstract public class TerrainGen : MonoBehaviour {

	public GameObject brick;
	public int resi,resj,resk; 
	// generates terrain
	public abstract void Generate (out float[,,] return1, out bool[,,] return2);
	public abstract void Create (out GameObject[,,] return3);

}
