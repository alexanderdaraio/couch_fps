﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainBasic : TerrainGen {


	public override void Generate (out float[,,] return1, out bool[,,] return2) {

		return1 = new float[resi,resi,resi];
		return2 = new bool[resi,resi,resi];

		for (int i=0;i<resi;i++)	{	
			for (int j=0;j<resi;j++)	{
				for (int k=0;k<resi;k++)	{
					// effectvely just makes a grid floor of 2
					if (j <= 2) {
						return1 [i, j, k] = 1f;
						return2 [i, j, k] = true;
					}
				}
			}
		}
	}

	public override void Create (out GameObject[,,] return3) {

		GameObject ground;

		return3 = new GameObject[resi,resi,resi];

		for (int i=0;i<resi;i++)	{	
			for (int j=0;j<resi;j++)	{
				for (int k=0;k<resi;k++)	{
					// effectvely just makes a grid floor of 2
					if (GameData2.Data.GetBrick (i, j, k)) {
						//even hex
						if (i % 2 == 0) {
							ground = Instantiate (brick, new Vector3 (1.817f * (float)i, j, 2 * (float)k + 1), transform.rotation) as GameObject;
						}
						//odd hex
						else {
							ground = Instantiate (brick, new Vector3 (1.817f * (float)i, j, 2 * (float)k), transform.rotation) as GameObject;
						}
						ground.GetComponentInChildren<Hex2> ().SetPos (i, j, k, resi, resj,resk, GameData2.Data.GetBrickValue(i,j,k));
						return3 [i, j, k] = ground;
					}
				}
			}
		}
	}

}
