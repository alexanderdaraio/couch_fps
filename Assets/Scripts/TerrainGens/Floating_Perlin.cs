﻿using UnityEngine;
using System.Collections;

public class Floating_Perlin : MonoBehaviour {
	/*Things to DO:
	 * 1. Create random removal on the edges to remove flat planes.. maybe smooth will help?
	 * Good Game Idea with 3D...Make the player land on the 3d environment... but not allowed to fall off, must survive onslaught of enemies. Can dig, and pick up materials to place from dead enemies
	 * Hardness determined by noise settings (basically how good the terrain is) 
	 * 2. Make non visible blocks not render
	 **/



	//resolution of cube
	public int res = 17;
	// frequency of the largest amplitude
	public int minFreq = 2; // must be 2^x
	// this sets the level of detail... a gets irrelevan dealing with bricks
	public int octaves = 4; 
	// higher is bumpier... too low on blocks wont show... 1/4, 1/2, 1/root2, 1
	public float persistance = .5f; 
	//tolerance -ive means more blocks, +ive means more space
	public float tolerance = 0;
	//tolerance -ive means more blocks, +ive means more space
	public float fenceTolerance = .5f;
	// random factor, lower is more random... impliment this to make it more varied... think about probabilties (using random ranges)
	//public float randomFactor = 1;
	// the Cube
	public GameObject floor;
	private float[,,] totalHeights;
	private int[,,] smoothedTotalHeights;
	private GameObject ground;

	void Start () {
		GameData.Data.Setup (res);
		PerlinNoise();
		SetHeight();
		//SetRender ();
	}

	void PerlinNoise() {
		// this is to set initial octave random points. Total can never be a decimal as it is coordinates
		int total = (res - 1)/minFreq;

		float[,,,] heights = new float[octaves,res,res,res];
		float[,,,] smoothedHeights = new float[octaves,res,res,res];

		totalHeights = new float[res,res,res];
		smoothedTotalHeights= new int[res,res,res];

		for (int n=0;n<octaves && total>=1;n++)
		{

			// each point on the octave
			for (int i=0;i<res;i = i + total)
			{	
				for (int j=0;j<res;j = j + total)
				{
					for (int k=0;k<res;k = k + total)
					{
						//weighting
						heights[n,i,j,k] = Random.Range(-1f,1f)/Mathf.Pow(1/persistance,(float)n);
					}
				}
			}

			//interpolating... gets tricky haha
			for (int x=0;x<res-total;x= x + total)
			{	
				for (int y=0;y<res-total;y = y + total)
				{
					for (int z=0;z<res-total;z = z + total)
					{
						float p1 = heights[n,x,y,z];
						float p2 = heights[n,x + total,y,z];

						float p3 = heights[n,x,y+total,z];
						float p4 = heights[n,x+total,y+total,z];


						float p5 = heights[n,x,y,z+total];
						float p6 = heights[n,x+total,y,z+total];

						float p7 = heights[n,x,y+total,z+total];
						float p8 = heights[n,x+total,y+total,z+total];

						for(int i=0;i<=total;i++)
						{
							for(int j=0;j<=total;j++)
							{
								for(int k=0;k<=total;k++)
								{
									float i1 = CosineInterpolate(p1,p2, (float)i/(float)total);
									float i2 = CosineInterpolate(p3,p4, (float)i/(float)total);
									float i3 = CosineInterpolate(p5,p6, (float)i/(float)total);
									float i4 = CosineInterpolate(p7,p8, (float)i/(float)total);

									float j1 = CosineInterpolate(i1,i2, (float)j/(float)total);
									float j2 = CosineInterpolate(i3,i4, (float)j/(float)total);

									heights[n,x+i,y+j,z+k] = CosineInterpolate(j1,j2, (float)k/(float)total);

								}
							}
						}
					}
				}
			}
			total = total/2;

			// could be more efficient (could mix with set hight and interpolate etc)
			for (int i=0;i<res;i++)
			{	
				for (int j=0;j<res;j++)
				{
					for (int k=0;k<res;k++)
					{
						totalHeights[i,j,k] = totalHeights[i,j,k] + heights[n,i,j,k];
					}
				}
			}
		}
	}

	float CosineInterpolate(float a, float b, float x)

	{
		float ft = x*Mathf.PI;
		float f = (1f - Mathf.Cos(ft)) * .5f;
		return a*(1f-f)+b*f;
	}


	// sets where bricks will be
	void SetHeight(){
		int highest = 0;
		int highi, highj, highk;
		highi = highj = highk = 0;
		for (int i=0;i<res;i++)	{	
			for (int j=0;j<res;j++)	{
				for (int k=0;k<res;k++)	{

					// ADDED NEW FUNCTION HERE TO GENERATE TERRAIN WITH HIGHER PROBABLITY OF OCCURING LOWER AND NO ROOF
					//tolerance = ((float)(j)*2-((float)res))/((float)res);//***********effectively based on the distance from the bottom as the tolerance level. put this back if you want terrain (with floor and sky)
					tolerance = (Mathf.Abs(Vector3.Distance(new Vector3((((float)res-1f)/2),(((float)res-1f)/2),(((float)res-1f)/2)), new Vector3((float)i,(float)j,(float)k))) -(((float)res-1f)/4))/((((float)res-1f)/4)); //*********** Efectively the distance from the centre of the sphere as tolerance level. put this back if you want random asteroids

					if(totalHeights[i,j,k]>=tolerance)	{
						//even hex
						if (i % 2 == 0) {
							ground = Instantiate (floor, new Vector3 (1.817f*(float)i, j, 2*(float)k+1), transform.rotation) as GameObject;
						}
						//odd hex
						else {
							ground = Instantiate (floor, new Vector3 (1.817f*(float)i, j, 2*(float)k), transform.rotation) as GameObject;
						}
						GameData.Data.SetBrick (i, j, k, true);
						ground.GetComponentInChildren<Hex> ().SetPos (i, j, k, res,totalHeights [i, j, k]);
						// checks if it should spawn flag
						if (j > highest) {
							highest = j;
							highi = i;
							highj = j;
							highk = k;
						}

					}
				}
			}
		}
		GameData.Data.SetFlag (highi, highj, highk);
	}



}
