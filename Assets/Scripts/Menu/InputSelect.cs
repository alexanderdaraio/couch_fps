﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputSelect : MonoBehaviour {
	//declaring it static as i believe these can be referenced cross scene (allowing us to check splitscreen players in the game)
	public static InputSelect inputSelect;
	public Canvas canvas;
	public bool[] playersIn;

	// Use this for initialization
	void Start () {
		inputSelect = this;
		playersIn = new bool[5];
	}
	
	// Update is called once per frame
	void Update () {
		for (int i = 1; i <= 4; i++) {
			if (Input.GetKeyUp ("joystick "+i+" button 7")) {
				canvas.transform.GetChild (0).GetChild (2).GetChild(i-1).GetComponent<Text>().text = "true";
				playersIn [i-1] = true;
			}
		}
		for (int i = 1; i <= 4; i++) {
			if (Input.GetKeyUp ("joystick "+i+" button 6")) {
				canvas.transform.GetChild (0).GetChild (2).GetChild(i-1).GetComponent<Text>().text = "false";
				playersIn [i-1] = false;
			}
		}
		if (Input.GetKeyUp ("return")) {
			canvas.transform.GetChild (0).GetChild (2).GetChild(4).GetComponent<Text>().text = "true";
			playersIn [4] = true;

		}
		if (Input.GetKeyUp ("escape")) {
			canvas.transform.GetChild (0).GetChild (2).GetChild(4).GetComponent<Text>().text = "false";
			playersIn [4] = false;

		}
	
	}
}
