﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {
	public float speed; 
	public float lifespan;


	void Start() {
		Rigidbody rb = GetComponent<Rigidbody> ();
		rb.velocity = transform.forward * speed;
	}
	// Update is called once per frame
	void FixedUpdate () {
		lifespan -= Time.deltaTime;
		//transform.position = transform.position + transform.forward * speed * Time.deltaTime;
		RaycastHit hit;
		if (Physics.Raycast (transform.position, transform.forward, out hit,speed * 2 *Time.deltaTime)) {
			//Debug.Log (hit.transform);
			//Destroy (gameObject);
			Explode();

		}
		if (lifespan < 0) {
			Destroy (gameObject);
		}
	}

	void Explode() {
		var exp = GetComponent<ParticleSystem>();
		exp.Play();
		GetComponent<Renderer> ().enabled = false;
		GetComponent<SphereCollider> ().enabled = false;
		Destroy(gameObject,2f);
	}

}
